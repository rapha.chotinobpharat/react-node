// console.log('Hello World');

// Variable scoping
// console.log('## start  Variable scoping ##');

var a = "Hello World"

function hi() {
    // console.log(a);

}

function hello() {
    let b = 'Hello'
}

// console.log(b);

hi();
hello(); // stack memmory new function();

// console.log('## end  Variable scoping ##');

const count = (a) => {
    // console.log('count', a);
};

const count1 = () => console.log('count1');
// new function 1 time

// count(4);

const array1 = [1, 2, 3];
// map
const mapA = array1.map(element => ('No ' + element));
// console.log(mapA);

// filter 

const filterA = array1.filter(element => element > 1);
// console.log(filterA);

// forEach
array1.forEach(element => {
    // console.log(element);
})

if (array1.length > 2) {
 // do somthing
} else {
    // do somthing
}

// short if
// array1.length > 2 ? console.log('if') : console.log('else');

const name  = 'gogh';
const age = 30;

const profile = {
    "name": "gogh",
    "age": 30,
}

// Lab 1